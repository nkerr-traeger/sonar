{
  "name": "SonarQube",
  "description": "Continuously inspect your codebase with [SonarQube](https://www.sonarqube.org/)",
  "template_path": "templates/gitlab-ci-sonar.yml",
  "kind": "analyse",
  "prefix": "sonar",
  "is_component": true,
  "variables": [
    {
      "name": "SONAR_SCANNER_IMAGE",
      "description": "The Docker image used to run [sonar-scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/)",
      "default": "registry.hub.docker.com/sonarsource/sonar-scanner-cli:latest"
    },
    {
      "name": "SONAR_HOST_URL",
      "type": "url",
      "description": "SonarQube server url",
      "mandatory": true
    },
    {
      "name": "SONAR_PROJECT_KEY",
      "description": "SonarQube Project Key (might also be set in the `sonar-project.properties` file)",
      "advanced": true
    },
    {
      "name": "SONAR_PROJECT_NAME",
      "description": "SonarQube Project Name (might also be set in the `sonar-project.properties` file)",
      "advanced": true
    },    
    {
      "name": "SONAR_TOKEN",
      "description": "SonarQube authentication token (depends on your authentication method)",
      "secret": true
    },
    {
      "name": "SONAR_LOGIN",
      "description": "SonarQube login (depends on your authentication method)",
      "secret": true
    },
    {
      "name": "SONAR_PASSWORD",
      "description": "SonarQube password (depends on your authentication method)",
      "secret": true
    },
    {
      "name": "SONAR_BASE_ARGS",
      "description": "SonarQube [analysis arguments](https://docs.sonarqube.org/latest/analysis/analysis-parameters/)",
      "default": "-Dsonar.links.homepage=${CI_PROJECT_URL} -Dsonar.links.ci=${CI_PROJECT_URL}/-/pipelines -Dsonar.links.issue=${CI_PROJECT_URL}/-/issues",
      "advanced": true
    },
    {
      "name": "SONAR_QUALITY_GATE_ENABLED",
      "description": "Enables SonarQube [Quality Gate](https://docs.sonarqube.org/latest/user-guide/quality-gates/) verification.\n\n_Uses `sonar.qualitygate.wait` parameter ([see doc](https://docs.sonarqube.org/latest/analysis/ci-integration-overview/#header-1))._",
      "type": "boolean"
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-sonar-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:master",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url"
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
